//
//  FZTableViewController.m
//  FZTableViewTest
//
//  Created by Fan Zhang on 6/9/14.
//  Copyright (c) 2014 Fan Zhang. All rights reserved.
//

#import "FZTableViewController.h"

@interface FZTableViewController ()

@end

@implementation FZTableViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _dataArray = [NSArray arrayWithObjects:
                  @"As stated on the home page of my website, GrammarBook.com represents American English rules. Rule number 1 of quotation marks is that periods and commas always go inside quotation marks. You will have a very hard time finding any American reference books on punctuation that will advise otherwise. Whether you are writing formal English in America or anywhere else in the world, you capitalize the first word of every sentence.",
                  @"I just wanted to say that Jane is correct. ",
                  @"Periods and commas always go within the closing quotation marks because, in typesetting in the 1800s, the pieces of type for the comma and period were the most fragile and could easily break. Putting them within quotation marks — even when it isn’t logical — protected them. This is why this is often called typesetters’ rules.",
                  @"It would seem that the right or wrong of this grammar rule is influenced by who you are writing to. This is similar in principal to the use of certain words such as labour vs labor, amongst vs among, or shall vs will–King’s English vs American English. I have done editing for both British and American publications, and I go by their respective rules.",
                  @"I’m trying to find out how to punctuate a book with a subtitle. I’ve normally seen subtitles with a colon; however, there is no punctuation in the actual title of the book on the cover since it is on a separate line. In writing the title with both on the same line, how should I separate the two?", nil];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return _dataArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.textLabel.text = _dataArray[indexPath.row];
    return cell;
}

@end
